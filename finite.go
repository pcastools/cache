// Finite is a cache of fixed size used for storing unordered data. It will flush automatically when full, and on close.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package cache

import (
	"fmt"
	"runtime/debug"
	"sync"
	"sync/atomic"
)

// Finite is a cache of fixed size used for storing unordered data. It will flush automatically when full, and on close.
type Finite[T any] struct {
	C      <-chan T         // Objects can be pulled from the cache via this channel. This channel will be closed when the cache is closed. Note that a cache is unordered: the order in which objects are recovered from the cache via this channel has no relation to the order in which they were added to the cache.
	inputC chan<- T         // Place an object on the cache.
	flushC chan<- int64     // Ask the worker to flush the cache.
	doneC  <-chan struct{}  // Closed by the worker on exit.
	w      *finiteWorker[T] // The background worker.
	mopen  sync.RWMutex     // Controls access to the following.
	isOpen bool             // Are we open (that is, have we not closed)?
}

// element wraps an element of type T
type element[T any] struct {
	e T
}

// finiteWorker is the background worker that maintains a Finite cache.
type finiteWorker[T any] struct {
	flushDoneC <-chan struct{} // Close to indicate flush has completed.
	cache      []*element[T]   // The cache of objects.
	merr       sync.Mutex      // Controls access to flushErr.
	flushErr   error           // The last error during flush.
	size       int64           // The number of elements in the cache.
}

// FlushFunc defines a function to be used to flush the cache.
type FlushFunc[T any] func([]T) error

// Error messages
const (
	msgUninitialisedCache = "attempting to push an object onto an uninitialised cache"
	msgPushWhenClosed     = "attempting to push an object onto a closed cache"
	msgIllegalCacheSize   = "the cache size must be a positive integer"
	msgNilFlushFunc       = "the flush function cannot be nil"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// drain drains the given channel. All objects received will be dropped.
func drain[T any](c <-chan T) {
	for range c {
	}
}

// execFlushFunc executes the flush function, recovering from any panics.
func execFlushFunc[T any](f FlushFunc[T], S []T) (err error) {
	// Recover from any panics caused by the user's flush function f
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("panic in FlushFunc: %s\n%s", e, debug.Stack())
		}
	}()
	// Execute the flush function
	err = f(S)
	return
}

/////////////////////////////////////////////////////////////////////////
// finiteWorker functions
/////////////////////////////////////////////////////////////////////////

/*
Note on w.size:
w.size can only be modified by the worker, run(), which runs in its own go routine. There is no need for the worker to read from w.size atomically. However, other go routines (owned by the user) will need to read from w.size. Hence those reads external to the work should be atomic, and any modifications of w.size by the worker should be atomic.
*/

// push adds the given element to the end of the cache of objects and increments the count of the number of objects on the cache. Assumes that there is space in the cache. Returns true if the cache needs flushing. Only intended to be called from the worker's go-routine.
func (w *finiteWorker[T]) push(x T) bool {
	w.cache[int(w.size)] = &element[T]{e: x}
	atomic.AddInt64(&w.size, 1) // Do this atomically
	return int(w.size) == len(w.cache)
}

// prune decrements the count of the number of objects on the cache. Only intended to be called from the worker's go-routine.
func (w *finiteWorker[_]) prune() {
	atomic.AddInt64(&w.size, -1) // Do this atomically
	w.cache[int(w.size)] = nil
}

// peak returns the final element in the cache of objects. If the cache is empty, the nil object will be returned. The second return value will be true iff the cache was non-empty. The object will not be removed from the cache -- call w.prune() to remove the object. Only intended to be called from the worker's go-routine.
func (w *finiteWorker[T]) peak() (*element[T], bool) {
	if w.size == 0 {
		return nil, false
	}
	return w.cache[int(w.size)-1], true
}

// flush flushes all but N of the cache of objects using the flush function. Only intended to be called from the worker's go-routine.
func (w *finiteWorker[T]) flush(N int64, flushFunc FlushFunc[T]) error {
	// If a flush is running, block until it finishes
	if w.flushDoneC != nil {
		<-w.flushDoneC
		w.flushDoneC = nil
	}
	// Check for errors before continuing
	if err := w.Err(); err != nil {
		return err
	}
	// Is there anything to do?
	if N < 0 {
		N = 0
	}
	if w.size <= N {
		return nil
	}
	// Records the size and N as ints for convenience
	size := int(w.size)
	n := int(N)
	// Make a copy of the data we need to flush, and zero it out
	S := make([]T, 0, size-n)
	for i := n; i < size; i++ {
		S = append(S, w.cache[i].e)
		w.cache[i] = nil
	}
	// Execute the flush function in its own go-routine
	flushDoneC := make(chan struct{})
	go func(flushDoneC chan<- struct{}) {
		if err := execFlushFunc(flushFunc, S); err != nil {
			w.SetError(err)
		}
		close(flushDoneC)
	}(flushDoneC)
	w.flushDoneC = flushDoneC
	// Update the count
	atomic.StoreInt64(&w.size, N) // Do this atomically
	return nil
}

// waitForInput blocks until an object is received over the input channel (returns x,true,false,0), or the input channel closes (returns nil,false,false,0), or a request to flush the cache is received (returns nil,false,true,N). Whilst waiting for input, this will (non-blockingly) try to push a objects from the cache onto the output channel. Only intended to be called from the worker's go-routine.
func (w *finiteWorker[T]) waitForInput(inputC <-chan T, outputC chan<- T, flushC <-chan int64) (x T, gotObject bool, shouldFlush bool, N int64) {
	y, hadObject := w.peak()
	for hadObject {
		select {
		case x, gotObject = <-inputC:
			return
		case N, shouldFlush = <-flushC:
			return
		case outputC <- y.e:
			w.prune()
			y, hadObject = w.peak()
		}
	}
	select {
	case x, gotObject = <-inputC:
	case N, shouldFlush = <-flushC:
	}
	return
}

// run starts the worker. Objects are read from the channel inputC and placed on the cache. Objects are removed from the cache and fed down the outputC channel. The cache will flush and the worker exit when the inputC channel is closed. The worker will also exit on error. The cache will automatically flush when full; the cache will also flush when receiving a message down the flushC channel. The outputC channel will be closed by the worker during the exit process. The worker will close the doneC channel as the final act before exiting. Intended to be run in its own go-routine.
func (w *finiteWorker[T]) run(inputC <-chan T, outputC chan T, flushC <-chan int64, doneC chan<- struct{}, flushFunc FlushFunc[T]) {
	// Defer closing the doneC channel
	defer close(doneC)
	// Loop until the inputC channel closes
	x, gotObject, shouldFlush, N := w.waitForInput(inputC, outputC, flushC)
	for gotObject || shouldFlush {
		// Do we need to add an object to the cache?
		if gotObject {
			if shouldFlush = w.push(x); shouldFlush {
				N = w.size / 2
			}
		}
		// Do we need to flush the cache?
		if shouldFlush && w.flush(N, flushFunc) != nil {
			// There was an error flushing -- we need to exit
			close(outputC)
			go drain(inputC)
			go drain(flushC)
			return
		}
		// Move on
		x, gotObject, shouldFlush, N = w.waitForInput(inputC, outputC, flushC)
	}
	// Ensure that the flush channel is drained
	go drain(flushC)
	// Drain the output channel, returning the contents to the cache
	close(outputC)
	for x := range outputC {
		if w.push(x) && w.flush(0, flushFunc) != nil {
			return
		}
	}
	// Flush the cache before exiting
	w.flush(0, flushFunc)
	if w.flushDoneC != nil {
		<-w.flushDoneC
	}
}

// Len returns the number of objects currently in the cache.
func (w *finiteWorker[_]) Len() int {
	size := atomic.LoadInt64(&w.size) // Do this atomically
	return int(size)
}

// SetError sets the error (if not set).
func (w *finiteWorker[_]) SetError(err error) {
	w.merr.Lock()
	if w.flushErr == nil {
		w.flushErr = err
	}
	w.merr.Unlock()
}

// Err returns the set error (if any).
func (w *finiteWorker[_]) Err() error {
	w.merr.Lock()
	err := w.flushErr
	w.merr.Unlock()
	return err
}

/////////////////////////////////////////////////////////////////////////
// Finite functions
/////////////////////////////////////////////////////////////////////////

// Len returns the number of objects currently in the cache.
func (c *Finite[_]) Len() (n int) {
	if c != nil {
		n = c.w.Len()
	}
	return
}

// SetError places the cache in an error state.
func (c *Finite[_]) SetError(err error) {
	if c != nil && err != nil {
		c.w.SetError(err)
	}
}

// Err returns the any error.
func (c *Finite[_]) Err() (err error) {
	if c != nil {
		err = c.w.Err()
	}
	return
}

// Add places x onto the cache.
func (c *Finite[T]) Add(x T) error {
	if c == nil {
		panic(msgUninitialisedCache)
	}
	c.mopen.RLock()
	if !c.isOpen {
		c.mopen.RUnlock()
		panic(msgPushWhenClosed)
	} else if err := c.Err(); err != nil {
		c.mopen.RUnlock()
		return err
	}
	c.inputC <- x
	c.mopen.RUnlock()
	return nil
}

// FlushAllButN flushes all but N elements from the cache.
func (c *Finite[_]) FlushAllButN(N int) {
	if N < 0 {
		N = 0
	}
	if c != nil {
		c.mopen.RLock()
		if !c.isOpen || c.Err() != nil {
			c.mopen.RUnlock()
			return
		}
		c.flushC <- int64(N)
		c.mopen.RUnlock()
	}
}

// Flush flushes the cache.
func (c *Finite[_]) Flush() {
	c.FlushAllButN(0)
}

// Close closes the cache. Any cached data will be flushed.
func (c *Finite[_]) Close() error {
	if c == nil {
		return nil
	}
	c.mopen.Lock()
	if c.isOpen {
		// Mark us as closed
		c.isOpen = false
		// Shut down the background worker (this causes a complete flush)
		close(c.inputC)
		<-c.doneC
		close(c.flushC)
	}
	c.mopen.Unlock()
	return c.Err()
}

// NewFinite returns a new finite cache of maximum size maxSize using the given flush function.
func NewFinite[T any](maxSize int, flush FlushFunc[T]) *Finite[T] {
	// Sanity check
	if maxSize <= 0 {
		panic(msgIllegalCacheSize)
	} else if flush == nil {
		panic(msgNilFlushFunc)
	}
	// Create the communication channels
	inputC := make(chan T)
	outputC := make(chan T)
	flushC := make(chan int64)
	doneC := make(chan struct{})
	// Create and start the background worker
	w := &finiteWorker[T]{
		cache: make([]*element[T], maxSize),
	}
	go w.run(inputC, outputC, flushC, doneC, flush)
	// Return the cache
	return &Finite[T]{
		C:      outputC,
		inputC: inputC,
		flushC: flushC,
		doneC:  doneC,
		w:      w,
		isOpen: true,
	}
}
